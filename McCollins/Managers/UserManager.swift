//
//  UserManager.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Foundation

class UserManager {
    
    let kUserLogged = "userLogged"
    let kUserEmail = "userEmail"
    
    static let user = UserManager()
    
    private init() {}
    
    func signIn() {
        UserDefaults.standard.set(true, forKey: kUserLogged)
    }
    
    func signOut() {
        clearUserEmail()
        UserDefaults.standard.set(false, forKey: kUserLogged)
    }
    
    func isLogged() -> Bool {
        return UserDefaults.standard.bool(forKey: kUserLogged)
    }
    
    func setUserEmail(with email: String) {
        UserDefaults.standard.set(email, forKey: kUserEmail)
    }
    
    func getCurrentUserEmail() -> String {
        return UserDefaults.standard.string(forKey: kUserEmail) ?? ""
    }
    
    fileprivate func clearUserEmail() {
        UserDefaults.standard.set("", forKey: kUserEmail)
    }
    
}
