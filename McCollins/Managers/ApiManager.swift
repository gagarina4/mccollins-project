//
//  ApiManager.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Alamofire
import SDWebImage

typealias JSONResponse = [String: AnyObject]

class ApiManager {
    
    static let shared = ApiManager()
    private init() {}
    
    func userRegistration(with params: Parameters, completion: @escaping ((_ success: Bool) -> ())) {
        callMethodWith(path: "registerUser", method: .post, parameters: params) { (response, success) in
            completion(success)
        }
    }
    
    func userLogin(with params: Parameters, completion: @escaping ((_ success: Bool) -> ())) {
        callMethodWith(path: "checklogin", method: .post, parameters: params) { (response, success) in
            completion(success)
        }
    }
    
    func getUser(with params: Parameters, completion: @escaping ((_ user: [String: Any], _ success: Bool) -> ())) {
        callMethodWith(path: "getUser", method: .post, parameters: params) { (response, success) in
            if success {
                guard let responseJSON = response.result.value as? JSONResponse, let value = responseJSON["data"] as? [JSONResponse] else {
                    print("Invalid information from service")
                    completion([:], false)
                    return
                }
                completion (value[0], true)
            } else {
                completion ([:], false)
            }
        }
    }
    
    func updateUser(with params: Parameters, completion: @escaping ((_ success: Bool) -> ())) {
        callMethodWith(path: "updateUser", method: .post, parameters: params) { (response, success) in
            completion(success)
        }
    }
    
    func getListAttractions(with params: Parameters, completion: @escaping ((_ success: Bool, _ data: [AttractionModel]) -> ())) {
        callMethodWith(path: "listAttractions", method: .post, parameters: params) { (response, success) in
            if success {
                if let data = response.data {
                    do {
                        let result = try JSONDecoder().decode(AttractionResultModel.self, from: data)
                        let attractionResults = result.data
                        completion(true, attractionResults)
                    } catch let jsonErr {
                        print("Error serialization json:", jsonErr)
                        completion(false, [])
                    }
                }
            } else {
                completion(false, [])
            }
        }
    }
    
    //MARK:- Fileprivate
    
    fileprivate let baseURL = "http://mccollinsmedia.com/myproject/service/"
    
    fileprivate func callMethodWith(path: String, method: HTTPMethod, parameters: Parameters, completion: @escaping (_ jsonResponse: DataResponse<Any>, _ success: Bool) -> ()) {
        let url = baseURL + path
        print(parameters)
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            guard response.result.isSuccess else {
                print("Request error")
                completion(response, false)
                return
            }
            guard let responseJSON = response.result.value as? JSONResponse else {
                print("Invalid information from service")
                completion(response, false)
                return
            }
            print("ResponseJSON -> \(responseJSON)")
            guard responseJSON["iserror"] as? String != "Yes" else {
                print("Get error from service")
                completion(response, false)
                return
            }
            completion(response, true)
        }
    }
    
}
