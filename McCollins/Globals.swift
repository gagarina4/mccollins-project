//
//  Globals.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit

typealias UIAlertActionHandler = (UIAlertAction) -> Void

//MARK: - methods

func alert(_ message: String, title: String = "Error", onClose: UIAlertActionHandler? = nil) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: onClose))
    return alert
}

func showAlert(_ message: String, title: String = "Error", onClose: UIAlertActionHandler? = nil) {
    if let viewController = UIApplication.topViewController() {
        viewController.present(alert(message, title: title, onClose: onClose), animated: true, completion: nil)
    }
}

//MARK: - extensions

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        return base
    }
}
