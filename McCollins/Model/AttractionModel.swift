//
//  AttractionModel.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Foundation

struct AttractionResultModel: Decodable {
    let iserror: String?
    let data: [AttractionModel]
}

struct AttractionModel: Decodable, ProducesAttractionViewModel {
    
    let title: String
    let timing: String?
    let contact: String?
    let description: String?
    let image: String?
    let latitude: String?
    let longitude: String?
    let sitelink: String?
    
    func convertToAttractionViewModel() -> AttractionViewModel {
        return AttractionViewModel(title: title, timing: timing, contact: contact, description: description, imageUrl: image, latitude: latitude, longitude: longitude, sitelink: sitelink)
    }
    
}
