//
//  UserModel.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Foundation

struct UserModel {

    var fname: String?
    var lname: String?
    var email: String?
    var mobile: String?
    var dob: String?
    var image: String?
    var gender: String?
    
    init(dictionary: [String: Any]) {
        self.fname = dictionary["fname"] as? String
        self.lname = dictionary["lname"] as? String
        self.email = dictionary["email"] as? String
        self.mobile = dictionary["mobile"] as? String
        self.dob = dictionary["dob"] as? String
        self.image = dictionary["image"] as? String
        self.gender = dictionary["gender"] as? String
    }
    
}
