//
//  HomeController.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit
import CoreLocation
import JGProgressHUD

//protocol LoginControllerDelegate {
//    func didFinishLoggingIn()
//}

class HomeController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    let cellIdentifier = "AttractionCell"
    var dataSource: [AttractionViewModel] = []
    
    let tableView: UITableView = {
        let tb = UITableView(frame: .zero)
        tb.backgroundColor = UIColor(white: 0.95, alpha: 1)
        tb.separatorStyle = .none
        return tb
    }()
    
    lazy var locationManager: CLLocationManager = {
        var lm = CLLocationManager()
        lm.delegate = self
        lm.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        lm.distanceFilter = 10.0
        return lm
    }()
    
    var updateLocationTimer: Timer!
    
    //MARK:- LifeCircle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationItems()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserManager.user.isLogged() {
            self.locationManager.requestAlwaysAuthorization()
            updateLocationTimer = Timer.scheduledTimer(timeInterval: 20, target:self, selector: #selector(trackLocation), userInfo: nil, repeats: true)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserManager.user.isLogged() {
            fetchAttractions()
        } else {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.showAutorisationScreen(from: self)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateLocationTimer?.invalidate()
    }
    
    //MARK:- Fileprivate
    
    //MARK: - Location methods
    
    @objc fileprivate func trackLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        dataSource.forEach{ $0.getDistanceValue(userLocation: locations[0]) }
    }
    
    //MARK:- FetchData

    fileprivate func fetchAttractions() {
        let email = UserManager.user.getCurrentUserEmail()
        let params: [String : Any] = ["email": email]
        let loginHUD = JGProgressHUD(style: .dark)
        loginHUD.textLabel.text = "Fetching data..."
        loginHUD.show(in: self.view)
        ApiManager.shared.getListAttractions(with: params) { (success, data) in
            loginHUD.dismiss()
            if !success {
                showAlert("Get loading error")
                return
            }
            self.dataSource = data.map({ $0.convertToAttractionViewModel() })
            self.tableView.reloadData()
            self.locationManager.startUpdatingLocation()
        }
    }
    
    //MARK:- UITableView
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AttractionCell
        cell.attractionViewModel = dataSource[indexPath.row]
        cell.phoneBtnObserver = { [weak self] (phoneNumber) in
            self?.makeCall(number: phoneNumber)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentAttractionVM = dataSource[indexPath.row]
        handleGoToAttractionDetails(withLink: currentAttractionVM.sitelink ?? "")
    }
    
    //MARK:- SetupLayout
    
    fileprivate func setupNavigationItems() {
        navigationController?.navigationBar.isTranslucent = false;
        navigationController?.navigationBar.barTintColor = UIColor.color_blueLight()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.title = "Home"
        
        view.backgroundColor = .white
        
        let rightBarButtonItem = UIBarButtonItem(title: "Profile", style: .plain, target: self, action: #selector(handleProfile))
        rightBarButtonItem.tintColor = .white
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    //MARK:- Button Actions
    
    fileprivate func makeCall(number: String) {
        let alert = UIAlertController(title: "Do you want to make a call?", message: "Phone Number: \(number)", preferredStyle: .actionSheet)
        let singleAction = UIAlertAction(title: "Yes", style: .default) { (act) in
            if let url = URL(string: "tel://\(number)"),
                UIApplication.shared.canOpenURL(url) {
                print ("Call to Number: \(url)")
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(singleAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc fileprivate func handleGoToAttractionDetails(withLink: String) {
        let attractionDetailsController = AttractionDetailsController()
        attractionDetailsController.sitelink = withLink
        navigationController?.pushViewController(attractionDetailsController, animated: true)
    }
    
    @objc fileprivate func handleProfile() {
        let profileController = ProfileController()
        profileController.delegate = self
        let navController = UINavigationController(rootViewController: profileController)
        present(navController, animated: true)
    }
    
}

extension HomeController: ProfileControllerDelegate {

    func didSaveProfile() {
//        fetchAttractions()
    }
    
}
