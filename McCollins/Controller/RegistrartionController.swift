//
//  RegistrartionController.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit
import JGProgressHUD

class RegistrartionController: UIViewController {
    
    //    var delegate: LoginControllerDelegate?
    
    let logoView: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 98).isActive = true
        return view
    }()
    
    let firstnameTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Enter First Name")
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    let lastnameTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Enter Last Name")
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    let mobileTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Enter Mobile Number")
        tf.keyboardType = .numberPad
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    let genderControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Male", "Female"])
        sc.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .heavy)], for: .normal)
        sc.clipsToBounds = true
        sc.layer.cornerRadius = customTextFieldHeight/2
        sc.layer.borderColor = UIColor.white.cgColor
        sc.layer.borderWidth = 1
        sc.tintColor = .white
        sc.heightAnchor.constraint(equalToConstant: customTextFieldHeight).isActive = true
        sc.addTarget(self, action: #selector(handleGenderChanged), for: .valueChanged)
        return sc
    }()
    
    let dateButton: RegisterButton = {
        let btn = RegisterButton(type: .system)
        btn.addTarget(self, action: #selector(handleDateOfBirth), for: .touchUpInside)
        btn.setupDatePlaceholderState()
        return btn
    }()
    
    let emailTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Enter Email")
        tf.keyboardType = .emailAddress
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = CustomTextField(placeholder: "Enter Password")
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleTextChanged), for: .editingChanged)
        return tf
    }()
    
    let registerButton: RegisterButton = {
        let btn = RegisterButton(type: .system)
        btn.setTitle("Register", for: .normal)
        btn.addTarget(self, action: #selector(handleRegistration), for: .touchUpInside)
        return btn
    }()
    
    let goToLoginButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Go to Login", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleGoToLogin), for: .touchUpInside)
        return btn
    }()
    
    //MARK:- LifeCircle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGradientLayer()
        setupLayout()
        setupKeyboadNotificationObservers()
        setupKeyboadDismissTapGesture()
        setupRegistrationViewModelObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Fileprivate
    
    //MARK:- Keyboard
    
    fileprivate func setupKeyboadDismissTapGesture() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapDismiss)))
    }
    
    @objc fileprivate func handleTapDismiss() {
        view.endEditing(true)
    }
    
    fileprivate func setupKeyboadNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDismiss), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc fileprivate func handleKeyboardShow(notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardFrame = value.cgRectValue
        let bottomSpace = view.frame.height - verticalStackView.frame.origin.y - verticalStackView.frame.height
        let difference = keyboardFrame.height - bottomSpace
        self.view.transform = CGAffineTransform(translationX: 0, y: -difference - 10)
    }
    
    @objc fileprivate func handleKeyboardDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.transform = .identity
        })
    }
    
    //MARK:- TextField Observer
    
    @objc fileprivate func handleTextChanged(textField: UITextField) {
        if textField == firstnameTextField {
            registrationViewModel.firstName = textField.text
        } else if textField == lastnameTextField {
                registrationViewModel.lastName = textField.text
        } else if textField == emailTextField {
            registrationViewModel.email = textField.text
        } else if textField == mobileTextField {
            registrationViewModel.mobile = textField.text
        } else {
            registrationViewModel.password = textField.text
        }
    }
    
    let registrationViewModel = RegistrationViewModel()
    let loginHUD = JGProgressHUD(style: .dark)
    
    fileprivate func setupRegistrationViewModelObserver() {
        registrationViewModel.isFormValidObserver = { [unowned self] (isFormValid) in
            if isFormValid {
                self.registerButton.setupEnableState()
            } else {
                self.registerButton.setupDisableState()
            }
        }
        
        registrationViewModel.isRegistrationObserver = { [unowned self] (isRegistering) in
            if isRegistering == true {
                self.loginHUD.textLabel.text = "Registration..."
                self.loginHUD.show(in: self.view)
            } else {
                self.loginHUD.dismiss()
            }
        }
    }
    
    //MARK:- SetupLayout
    
    fileprivate func setupGradientLayer() {
        let gradientLayer = GradientLayer()
        view.layer.addSublayer(gradientLayer)
        gradientLayer.frame = view.bounds
    }
    
    lazy var verticalStackView = UIStackView(arrangedSubviews: [logoView,
                                                                firstnameTextField,
                                                                lastnameTextField,
                                                                mobileTextField,
                                                                genderControl,
                                                                dateButton,
                                                                emailTextField,
                                                                passwordTextField,
                                                                registerButton])
    
    fileprivate func setupLayout() {
        navigationController?.isNavigationBarHidden = true
        
        view.backgroundColor = .white
        self.view.addSubview(verticalStackView)
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 10
        verticalStackView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 50, bottom: 0, right: 50))
        verticalStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        let logoImage = UIImageView(image: UIImage(named: "logo_main"))
        logoView.addSubview(logoImage)
        logoImage.anchor(top: logoView.topAnchor, leading: logoView.leadingAnchor, bottom: logoView.bottomAnchor, trailing: logoView.trailingAnchor)
        logoImage.contentMode = .scaleAspectFit
        
        view.addSubview(goToLoginButton)
        goToLoginButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    //MARK:- Button Actions
    
    @objc fileprivate func handleGenderChanged(segmentedControl: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            registrationViewModel.gender = "Male"
            segmentedControl.subviews.last?.backgroundColor = .clear
            break;
        case 1:
            registrationViewModel.gender = "Female"
            segmentedControl.subviews.first?.backgroundColor = .clear
            break;
        default:
            break;
        }
    }
    
    @objc fileprivate func handleDateOfBirth() {
        let calendar = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        comps.year = -5
        let maxDate = calendar.date(byAdding: comps, to: Date())
        comps.year = -99
        let minDate = calendar.date(byAdding: comps, to: Date())
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 180))
        datePicker.datePickerMode = .date
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        let dateChooserAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        dateChooserAlert.view.addSubview(datePicker)
        dateChooserAlert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: { [unowned self] action in
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let dateString = formatter.string(from: datePicker.date)
            self.registrationViewModel.dateOfBirth = dateString
            self.dateButton.setupDateState(dateString)
        }))
        let height: NSLayoutConstraint = NSLayoutConstraint(item: dateChooserAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.1, constant: 260)
        dateChooserAlert.view.addConstraint(height)
        self.present(dateChooserAlert, animated: true, completion: nil)
    }
    
    @objc fileprivate func handleRegistration() {
        registrationViewModel.performRegistration { (success) in
            self.loginHUD.dismiss()
            if !success {
                showAlert("Registration Failed")
                return
            }
            
            UserManager.user.signIn()
            self.dismiss(animated: true, completion: {
//                self.delegate?.didFinishLoggingIn()
            })
        }
    }
    
    @objc fileprivate func handleGoToLogin() {
        let loginController = LoginController()
//        loginController.delegate = delegate
        navigationController?.pushViewController(loginController, animated: true)
    }
    
}
