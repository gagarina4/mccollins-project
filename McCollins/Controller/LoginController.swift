//
//  LoginController.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit
import JGProgressHUD

class LoginController: UIViewController {
    
//    var delegate: LoginControllerDelegate?
    
//    LOGIN INFO!: email: ashik@gmail.com, pass: asd
    
    let logoView: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 98).isActive = true
        return view
    }()
    
    let usernameTextField: CustomTextField = {
        let tf = CustomTextField(placeholder: "Enter Username")
        tf.keyboardType = .emailAddress
        tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: CustomTextField = {
        let tf = CustomTextField(placeholder: "Enter Password")
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return tf
    }()
    
    let loginButton: RegisterButton = {
        let btn = RegisterButton(type: .system)
        btn.setTitle("Login", for: .normal)
        btn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return btn
    }()
    
    fileprivate let backToRegisterButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Go back", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return btn
    }()
    
    //MARK:- LifeCircle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGradientLayer()
        setupLayout()
        setupKeyboadNotificationObservers()
        setupKeyboadDismissTapGesture()
        setupLoginViewModelObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Fileprivate
    
    //MARK:- Keyboard
    
    fileprivate func setupKeyboadDismissTapGesture() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapDismiss)))
    }
    
    @objc fileprivate func handleTapDismiss() {
        view.endEditing(true)
    }
    
    fileprivate func setupKeyboadNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDismiss), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc fileprivate func handleKeyboardShow(notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardFrame = value.cgRectValue
        let bottomSpace = view.frame.height - verticalStackView.frame.origin.y - verticalStackView.frame.height
        let difference = keyboardFrame.height - bottomSpace
        self.view.transform = CGAffineTransform(translationX: 0, y: -difference - 10)
    }
    
    @objc fileprivate func handleKeyboardDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.transform = .identity
        })
    }
    
    //MARK:- TextField Observer
    
    @objc fileprivate func handleTextChange(textField: UITextField) {
        if textField == usernameTextField {
            loginViewModel.username = textField.text
        } else {
            loginViewModel.password = textField.text
        }
    }
    
    let loginViewModel = LoginViewModel()
    let loginHUD = JGProgressHUD(style: .dark)
    
    fileprivate func setupLoginViewModelObservers() {
        loginViewModel.isFormValidObserver = { [unowned self] (isFormValid) in
            if isFormValid {
                self.loginButton.setupEnableState()
            } else {
                self.loginButton.setupDisableState()
            }
        }
        
        loginViewModel.isLoggingInObserver = { [unowned self] (isRegistering) in
            if isRegistering == true {
                self.loginHUD.textLabel.text = "Login..."
                self.loginHUD.show(in: self.view)
            } else {
                self.loginHUD.dismiss()
            }
        }
    }
    
    //MARK:- SetupLayout
    
    fileprivate func setupGradientLayer() {
        let gradientLayer = GradientLayer()
        view.layer.addSublayer(gradientLayer)
        gradientLayer.frame = view.bounds
    }
    
    lazy var verticalStackView = UIStackView(arrangedSubviews: [logoView,
                                                                usernameTextField,
                                                                passwordTextField,
                                                                loginButton,
                                                                ])
    
    fileprivate func setupLayout() {
        navigationController?.isNavigationBarHidden = true
        
        view.backgroundColor = .white
        self.view.addSubview(verticalStackView)
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 10
        verticalStackView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 50, bottom: 0, right: 50))
        verticalStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        let logoImage = UIImageView(image: UIImage(named: "logo_main"))
        logoView.addSubview(logoImage)
        logoImage.anchor(top: logoView.topAnchor, leading: logoView.leadingAnchor, bottom: logoView.bottomAnchor, trailing: logoView.trailingAnchor)
        logoImage.contentMode = .scaleAspectFit
        
        view.addSubview(backToRegisterButton)
        backToRegisterButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    //MARK:- Button Actions
    
    @objc fileprivate func handleBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func handleLogin() {
        loginViewModel.performLogin { (success) in
            self.loginHUD.dismiss()
            if !success {
                showAlert("You have entered an invalid username or password", title: "Login Failed")
                return
            }
            
            UserManager.user.signIn()
            self.dismiss(animated: true, completion: {
                //                self.delegate?.didFinishLoggingIn()
            })
        }
    }
    
}
