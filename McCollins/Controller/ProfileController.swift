//
//  ProfileController.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProfileControllerDelegate {
    func didSaveProfile()
}

class CustomImagePickerController: UIImagePickerController {
    var imageButton: UIButton?
}

class ProfileController: UITableViewController {

    var delegate: ProfileControllerDelegate?
    
    lazy var imageButton = createButton(selector: #selector(handleSelectPhoto))
    
    func createButton(selector: Selector) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle("Select Photo", for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 8
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.imageView?.contentMode = .scaleAspectFill
        button.clipsToBounds = true
        return button
    }
    
    //MARK:- LifeCircle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationItems()
        tableView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .interactive
        
        fetchCurrentUser()
    }
    
    //MARK:- TableView
    
    lazy var header: UIView = {
        let header = UIView()
        header.addSubview(imageButton)
        let padding: CGFloat = 16
        imageButton.anchor(top: header.topAnchor, leading: header.leadingAnchor, bottom: header.bottomAnchor, trailing:  header.trailingAnchor, padding: .init(top: padding, left: padding, bottom: padding, right: padding))
        return header
    }()
    
    class HeaderLabel: UILabel {
        override func drawText(in rect: CGRect) {
            super.drawText(in: rect.insetBy(dx: 16, dy: 0))
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return header
        }
        let headerLabel = HeaderLabel()
        switch section {
        case 1:
            headerLabel.text = "First Name"
        case 2:
            headerLabel.text = "Last Name"
        case 3:
            headerLabel.text = "Email"
        case 4:
            headerLabel.text = "Mobile Number"
        case 5:
            headerLabel.text = "Gender"
        case 6:
            headerLabel.text = "Date of Birth"
        default:
            headerLabel.text = " "
        }
        headerLabel.font = UIFont.boldSystemFont(ofSize: 16)
        return headerLabel
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 240
        }
        return 40
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 0 : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ProfileCell(style: .default, reuseIdentifier: nil)
        switch indexPath.section {
        case 1:
            cell.textField.placeholder = "Enter First Name"
            cell.textField.text = userModel?.fname
        case 2:
            cell.textField.placeholder = "Enter Last Name"
            cell.textField.text = userModel?.lname
        case 3:
            cell.textField.placeholder = "Enter Email"
            cell.textField.text = userModel?.email
        case 4:
            cell.textField.placeholder = "Enter Mobile Number"
            cell.textField.text = userModel?.mobile
        case 5:
            cell.textField.placeholder = "Enter Gender"
            cell.textField.text = userModel?.gender
        case 6:
            cell.textField.placeholder = "Enter Date of Birth"
            cell.textField.text = userModel?.dob
        default:
            cell.textField.placeholder = " "
        }
        return cell
    }
    
    //MARK:- Fileprivate
    
    //MARK:- FetchData
    
    fileprivate var userModel: UserModel?
    
    fileprivate func fetchCurrentUser() {
        ApiManager.shared.getUser(with: ["id": 1]) { [unowned self] (user, success) in
            if success {
                self.userModel = UserModel(dictionary: user)
                self.loadUserPhotos()
                self.tableView.reloadData()
            } else {
                showAlert("User data fetching failed")
            }
        }
    }
    
    fileprivate func loadUserPhotos() {
        if let imageUrl = userModel?.image, let url = URL(string: imageUrl) {
            SDWebImageManager.shared().loadImage(with: url, options: .continueInBackground, progress: nil) { (image, _, _, _, _, _) in
                self.imageButton.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }
    }
    
    //MARK:- SetupLayout
    
    fileprivate func setupNavigationItems() {
        navigationController?.navigationBar.isTranslucent = false;
        navigationController?.navigationBar.barTintColor = UIColor.color_blueLight()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.title = "Profile"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        let leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        leftBarButtonItem.tintColor = .white
        navigationItem.leftBarButtonItem = leftBarButtonItem
        let rightSaveBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
        rightSaveBarButtonItem.tintColor = .white
        let rightLogoutBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        rightLogoutBarButtonItem.tintColor = .white
        
        navigationItem.rightBarButtonItems = [
            rightSaveBarButtonItem,
            rightLogoutBarButtonItem
        ]
    }
    
    //MARK:- Button Actions
    
    @objc func handleSelectPhoto(button: UIButton) {
        print("Select photo with button:", button)
        let imagePicker = CustomImagePickerController()
        imagePicker.delegate = self
        imagePicker.imageButton = button
        present(imagePicker, animated: true)
    }
    
    @objc fileprivate func handleSave() {
        let params: [String : Any] = [:]
        ApiManager.shared.updateUser(with: params) { (success) in
            if !success {
                showAlert(" - I did not send data as it said in the Task - ", title: "Save Profile Failed")
                return
            }
            print("Finished saving profile")
            self.dismiss(animated: true, completion: {
                self.delegate?.didSaveProfile()
            })
        }
    }
    
    @objc fileprivate func handleLogout() {
        UserManager.user.signOut()
        dismiss(animated: true)
    }
    
    @objc fileprivate func handleCancel() {
        dismiss(animated: true)
    }

}

extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let selectedImage = info[.originalImage] as? UIImage
        let imageButton = (picker as? CustomImagePickerController)?.imageButton
        imageButton?.setImage(selectedImage?.withRenderingMode(.alwaysOriginal), for: .normal)
        dismiss(animated: true)
    }
    
}
