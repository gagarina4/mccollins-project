//
//  AttractionDetailsController.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit
import WebKit
import JGProgressHUD

class AttractionDetailsController: UIViewController {

    var sitelink: String = ""
    
    fileprivate let loginHUD = JGProgressHUD(style: .dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationItems()
        let webView = UIWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        webView.delegate = self
        self.view.addSubview(webView)
        let url = URL(string: sitelink)
        webView.loadRequest(URLRequest(url: url!))
    }
    
    //MARK:- SetupLayout
    
    fileprivate func setupNavigationItems() {
        navigationController?.navigationBar.isTranslucent = false;
        navigationController?.navigationBar.barTintColor = UIColor.color_blueLight()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.title = "Attraction Details"
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .white
    }

}

extension AttractionDetailsController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("Start loading")
        self.loginHUD.textLabel.text = "Loading..."
        self.loginHUD.show(in: self.view)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Finished loading")
        self.loginHUD.dismiss()
    }
    
}
