//
//  AttractionCell.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit
import SDWebImage

class AttractionCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timingLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var phoneBtn: UIButton!
    
    var phoneBtnObserver: ((String) -> ())?
    
    var attractionViewModel: AttractionViewModel! {
        didSet {
            titleLabel.text = attractionViewModel.title
            timingLabel.text = attractionViewModel.timing
            descrLabel.text = attractionViewModel.description
            phoneBtn.isHidden = true
            if let contact = attractionViewModel.contact {
                if contact.count > 0 {
                    phoneBtn.isHidden = false
                    phoneBtn.setTitle(contact, for: .normal)
                }
            }
            distanceLabel.text = ""
            if let distance = attractionViewModel.distance {
                distanceLabel.text = distance
            }
            cellImage.sd_setImage(with: URL(string: attractionViewModel.imageLink ?? ""), placeholderImage: UIImage())
            setupAttractionViewModelObservers()
        }
    }
    
    fileprivate func setupAttractionViewModelObservers() {
        attractionViewModel.distanceObserver = { [weak self] (distance) in
            self?.distanceLabel.text = distance
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        cellImage.contentMode = .scaleAspectFill
        cellImage.clipsToBounds = true
        cellImage.layer.cornerRadius = 12
        bgView.layer.cornerRadius = 12

        phoneBtn.addTarget(self, action: #selector(phoneBtnPressed), for: .touchUpInside)
        phoneBtn.layer.cornerRadius = 12
        phoneBtn.contentEdgeInsets = UIEdgeInsets(top: 4, left: 6, bottom: 4, right: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        distanceLabel.text = ""
        cellImage.image = UIImage()
    }
    
    @objc func phoneBtnPressed() {
        phoneBtnObserver?(attractionViewModel.contact ?? "")
    }
    
}
