//
//  RegisterButton.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit

class RegisterButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setTitleColor(#colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1), for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
        backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        isEnabled = false
        heightAnchor.constraint(equalToConstant: customTextFieldHeight).isActive = true
        layer.cornerRadius = customTextFieldHeight/2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupEnableState() {
        backgroundColor = #colorLiteral(red: 0.2039215686, green: 0.6980392157, blue: 0.9098039216, alpha: 1)
        isEnabled = true
        setTitleColor(.white, for: .normal)
    }
    
    func setupDisableState() {
        backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        isEnabled = false
        setTitleColor(#colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1), for: .normal)
    }
    
    func setupDatePlaceholderState() {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        isEnabled = true
        setTitleColor(#colorLiteral(red: 0.7450980392, green: 0.7450980392, blue: 0.7647058824, alpha: 1), for: .normal)
        setTitle("Setup Date of Birth", for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 17)
        contentHorizontalAlignment = .left
        contentEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 10)
    }
    
    func setupDateState(_ date: String) {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        isEnabled = true
        setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        setTitle(date, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 17)
        contentHorizontalAlignment = .left
        contentEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 10)
    }
    
}
