//
//  CustomTextField.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit

let customTextFieldHeight: CGFloat = 40

class CustomTextField: UITextField {
    
    let padding: CGFloat
    let height: CGFloat
    
    init(padding: CGFloat = 16, placeholder: String, height: CGFloat = customTextFieldHeight) {
        self.padding = padding
        self.height = height
        super.init(frame: .zero)
        self.placeholder = placeholder
        layer.cornerRadius = height/2
        backgroundColor = .white
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override var intrinsicContentSize: CGSize {
        return .init(width: 0, height: customTextFieldHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

