//
//  GradientLayer.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import UIKit

class GradientLayer: CAGradientLayer {

    override init() {
        super.init()
        let topColor = #colorLiteral(red: 0.9764705882, green: 0.8941176471, blue: 0.5058823529, alpha: 1)
        let bottomColor = #colorLiteral(red: 0.9960784314, green: 0.4352941176, blue: 0.4823529412, alpha: 1)
        colors = [topColor.cgColor, bottomColor.cgColor]
        locations = [0, 1]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
