//
//  AttractionViewModel.swift
//  McCollins
//
//  Created by Anton on 03.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol ProducesAttractionViewModel {
    func convertToAttractionViewModel() -> AttractionViewModel
}

class AttractionViewModel {
    
    let title: String
    let timing: String?
    let contact: String?
    let description: String?
    let imageLink: String?
    let sitelink: String?
    var image: UIImage? { didSet { attractionImageObserver?(image) }}
    var distance: String? { didSet { distanceObserver?(distance) }}
    var attractionLocation: CLLocation?

    var attractionImageObserver: ((UIImage?) -> ())?
    var distanceObserver: ((String?) -> ())?
    
    init(title: String, timing: String?, contact: String?, description: String?, imageUrl: String?, latitude: String?, longitude: String?, sitelink: String?) {
        self.title = title
        self.timing = timing
        self.contact = contact
        self.description = description
        self.imageLink = imageUrl
        self.sitelink = sitelink
        if let latitude = latitude, let longitude = longitude {
            self.attractionLocation = CLLocation(latitude: (latitude as NSString).doubleValue, longitude: (longitude as NSString).doubleValue)
        }
    }

    func getDistanceValue(userLocation: CLLocation) {
        if let location = self.attractionLocation {
            let distanceInMeters = location.distance(from: userLocation)
            if distanceInMeters < 1000 {
                updateDistanceValue(newDistance: "\(String(format: "%.0f", distanceInMeters)) M")
            } else {
                updateDistanceValue(newDistance: "\(String(format: "%.1f", distanceInMeters/1000)) Km")
            }
        }
    }

    fileprivate func updateDistanceValue(newDistance: String) {
        if newDistance != self.distance {
            self.distance = newDistance
        }
    }

}
