//
//  RegistrationViewModel.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Foundation

class RegistrationViewModel {
    
    var firstName: String? { didSet { checkFormValidity() }}
    var lastName: String? { didSet { checkFormValidity() }}
    var email: String? { didSet { checkFormValidity() }}
    var dateOfBirth: String? { didSet { checkFormValidity() }}
    var password: String? { didSet { checkFormValidity() }}
    var mobile: String? { didSet { checkFormValidity() }}
    var gender: String? { didSet { checkFormValidity() }}
    
    var isFormValidObserver: ((Bool) -> ())?
    var isRegistrationObserver: ((Bool) -> ())?
    
    fileprivate func checkFormValidity() {
        let isFormValid = firstName?.isEmpty == false && lastName?.isEmpty == false && email?.isEmpty == false && password?.isEmpty == false && mobile?.isEmpty == false && gender?.isEmpty == false && dateOfBirth?.isEmpty == false && isEmailValid(email) && isGenderValid(gender)
        isFormValidObserver?(isFormValid)
    }
    
    fileprivate func isEmailValid(_ email: String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: email)
    }
    
    fileprivate func isGenderValid(_ gender: String?) -> Bool {
        return (gender == "Male" || gender == "Female") ? true : false
    }
    
    func performRegistration(completion: @escaping (Bool) -> ()) {
        guard let firstName = firstName, let lastName = lastName, let email = email, let password = password, let mobile = mobile, let dateOfBirth = dateOfBirth, let gender = gender else { return }
        isRegistrationObserver?(true)
        let params: [String : Any] = [
            "fname": firstName,
            "lname": lastName,
            "email": email,
            "mobile": mobile,
            "password": password,
            "cpassword": password,
            "dob": dateOfBirth,
            "gender": gender
        ]
        print ("Params: \(params)")
        ApiManager.shared.userRegistration(with: params) { (success) in
            print("Is Success: \(success)")
            if success {
                UserManager.user.setUserEmail(with: email)
            }
            completion(success)
        }
    }
    
}
