//
//  LoginViewModel.swift
//  McCollins
//
//  Created by Anton on 02.01.2019.
//  Copyright © 2019 InterviewTask. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    var username: String? { didSet { checkFormValidity() }}
    var password: String? { didSet { checkFormValidity() }}
    
    var isFormValidObserver: ((Bool) -> ())?
    var isLoggingInObserver: ((Bool) -> ())?
    
    fileprivate func checkFormValidity() {
        let isFormValid = username?.isEmpty == false && password?.isEmpty == false
        isFormValidObserver?(isFormValid)
    }
    
    func performLogin(completion: @escaping (Bool) -> ()) {
        guard let username = username, let password = password else { return }
        isLoggingInObserver?(true)
        let params: [String : Any] = [
            "email": username,
            "password": password
        ]
        ApiManager.shared.userLogin(with: params) { (success) in
            print("Is Success: \(success)")
            if success {
                UserManager.user.setUserEmail(with: username)
            }
            completion(success)
        }
    }
    
}
